# What Rack Reform Can Be

1. network appliance
   - with pcie ethernet card (4 ports)
   router, firewall, web server
   - with wifi card
   access point

2. network attached storage (NAS)
   - with pcie SATA card
     - fill with 2.5inch disks
   - with pcie NVMe card
     - fill with SSDs

3. desktop/workstation/pc
   - blobless with LS1028A and Nvidia Kepler GPU
   - or low energy, fast with bpi a311d processor

4. settop box
   - with bpi or rpi processor

5. web server
   - no addins needed, perhaps 2.5GBe ethernet card

6. in audio rack
   - not sure for what, maybe high end audio card/recording?
